import { API_URL } from '../constants';

export async function getDataFromApi() {
  const response = await fetch(API_URL, {
    headers: {
      'Content-type': 'application/json',
    },
    method: 'GET',
  }).then(resp => resp.json())
    .catch(error => error);

  return response;
}
