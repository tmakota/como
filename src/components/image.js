import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Image,
  Easing,
  Dimensions,
} from 'react-native';
import { styles } from './style';

const { height } = Dimensions.get('window');

export class AnimatedImage extends Component {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
  }

  state = {
    uri: null,
    offset: 0,
    duration: 0,
    left: 0,
  };

  componentWillMount() {
    const { uri, offset, duration, left } = this.props;
    this.setState({ uri, offset, duration, left }, () => this.animate());
  }

  animate() {
    const { duration } = this.state;
    this.animatedValue.setValue(0);
    Animated.timing(
      this.animatedValue,
      {
        toValue: 1,
        duration: duration * 50 || 2000,
        easing: Easing.linear,
      },
    ).start(() => this.animate());
  }

  render() {
    const { uri, offset, left } = this.state;

    const bottom = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [offset, height],
    });

    return (<Animated.View
      style={{ bottom, left, position: 'absolute' }}>
      <Image
        style={styles.image}
        source={{ uri }} />
    </Animated.View>
    );
  }
}

AnimatedImage.propTypes = {
  uri: PropTypes.string.isRequired,
  offset: PropTypes.number.isRequired,
  duration: PropTypes.number.isRequired,
  left: PropTypes.number.isRequired,
};
