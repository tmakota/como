import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Easing,
  Text,
  Switch,
  Dimensions,
} from 'react-native';
import { styles } from './style';

const { height } = Dimensions.get('window');

export class AnimatedToggle extends Component {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
  }

  state = {
    offset: 0,
    duration: 0,
    state: '',
    text: '',
    left: 0,
  };

  componentWillMount() {
    const { offset, duration, left, state, text } = this.props;
    this.setState({ offset, duration, left, state, text }, () => this.animate());
  }

  animate() {
    const { duration } = this.state;
    this.animatedValue.setValue(0);
    Animated.timing(
      this.animatedValue,
      {
        toValue: 1,
        duration: duration * 50 || 2000,
        easing: Easing.linear,
      },
    ).start(() => this.animate());
  }

  render() {
    const { offset, left, status, text } = this.state;

    const bottom = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [offset, height],
    });

    return (<Animated.View
      style={{ bottom, left, position: 'absolute' }}>
      <Switch value={status === 'on'} />
      <Text style={styles.text}>{ text }</Text>
    </Animated.View>
    );
  }
}

AnimatedToggle.propTypes = {
  offset: PropTypes.number.isRequired,
  duration: PropTypes.number.isRequired,
  left: PropTypes.number.isRequired,
  state: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};
