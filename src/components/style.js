import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 150,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 100,
    height: 100,
  },
  text: {
    color: '#000000',
    textAlign: 'center',
  },
  button: {
    padding: 10,
    width: 100,
  },
  refresh: {
    width: 100,
    padding: 10,
    backgroundColor: 'white',
    position: 'absolute',
    borderWidth: 1,
    top: 30,
    left: (width / 2) - 50,
  },
});
