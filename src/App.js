import React, { Component } from 'react';
import {
  View,
  ActivityIndicator,
  TouchableOpacity,
  Text,
} from 'react-native';
import { getDataFromApi } from './api';
import { AnimatedImage, AnimatedButton, AnimatedToggle } from './components';
import { styles } from './components/style';

export default class App extends Component {
  state = {
    data: null,
  };

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const data = await getDataFromApi();
    this.setState({ data });
  }

  parseImagesData = (images) => images.map(image =>
    (<AnimatedImage
      left={image.position_x}
      offset={image.position_y}
      duration={image.speed}
      key={`image_${image.position_x}${image.position_y}`}
      uri={image.url} />),
  );

  parseButtonData = (buttons) => buttons.map(button =>
    (<AnimatedButton
      left={button.position_x}
      offset={button.position_y}
      duration={button.speed}
      color={button.color}
      text={button.text}
      key={`button_${button.position_x}${button.position_y}`} />),
  );

  parseToggleData = (toggles) => toggles.map(toggle =>
    (<AnimatedToggle
      left={toggle.position_x}
      offset={toggle.position_y}
      duration={toggle.speed}
      text={toggle.text}
      state={toggle.state}
      key={`toggle_${toggle.position_x}${toggle.position_y}`} />),
  );

  render() {
    const { data } = this.state;

    if (!data || !data.length) {
      return (<View style={styles.container}>
        <ActivityIndicator />
      </View>);
    }

    const images = data.filter(item => item.type === 'image');
    const buttons = data.filter(item => item.type === 'button');
    const toggles = data.filter(item => item.type === 'toggle');

    return (
      <View style={styles.container}>
        { this.parseImagesData(images) }
        { this.parseButtonData(buttons) }
        { this.parseToggleData(toggles) }
        <TouchableOpacity
          style={styles.refresh}
          onPress={() => this.getData()}>
          <Text style={styles.text}>Refresh</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

