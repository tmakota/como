# Como animation test project

## Development Setup

# install
```bash
yarn
```

# linter
```bash
yarn lint
```

# start on android
```bash
yarn start:android
```

# start on ios
```bash
yarn start:ios
```
# step to make apk

```bash

yarn build:android

cd android/app/build/outputs/apk/

adb install ...(press tab for list of apk)
```